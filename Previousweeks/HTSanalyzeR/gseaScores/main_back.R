suppressPackageStartupMessages(library(HTSanalyzeR))
n_genes=100
#set.seed(0) 
#geneList=rnorm(n_genes)
geneList=seq_len(n_genes)
names(geneList)=sprintf('gene%03d', seq_len(n_genes))
#gene_set_size=20
#geneSet=names(geneList)[seq_len(10)]
geneSet=names(geneList)[seq_len(20)]
#geneList=geneList+runif(n_genes)
#geneList
ES=gseaScores(geneList, geneSet)
ES

nPermutations=20
#permScores=
#permutationPvalueCollectionGsea(permScores, dataScores) 
sapply(
  seq_len(nPermutations)
  , function(x) {
    names(geneList)=sample(names(geneList))
    gseaScores(geneList, geneSet)
  }
  )


#ecdf(
#  )#(gseaScores(geneList, geneSet)) 

