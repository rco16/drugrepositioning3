#!/usr/bin/env bash
# vim: set noexpandtab tabstop=2:

dataset="/0/DATA/0/matrix"
hd5file="zspc_n1328098x22268.gctx"
count="1,22268"

set -v
h5dump -d "$dataset" --count="$count" "$hd5file"
